<?php

namespace IpwSystems\MetazoApi\Tests;

use IpwSystems\MetazoApi\Client;
use IpwSystems\MetazoApi\Configuration;

class ClientWrapper extends Client
{
    public array $send;

    public function __construct()
    {
        $config = new Configuration([
            'endpoint' => 'https://test',
            'key' => 'x',
            'user' => 'xx',
            'password' => 'xxx',
        ]);

        parent::__construct($config);
        $this->token = 'xx00xx';
    }

    protected function makeRequest(string $requestMethod, string $url, array $options = [])
    {
        $parts = parse_url($url);
        if (isset($parts['query'])) {
            parse_str($parts['query'], $query);
            $options['query'] = $query;
        }

        $options['method'] = $requestMethod;
        $this->send = $options;

        $data = ['success' => 'true'];

        switch ($parts['path']) {
            case '/authenticate':
                $data['token'] = 'xx00xx';
                break;

            case '/ping':
                $data['ping'] = 'pong';
                break;

            case '/datatypes':
                break;
        }

        $faker = new class() {
            public static $options;
            public function toArray() {
                return self::$options;
            }
        };

        $faker::$options = $data;
        return $faker;
    }
}
