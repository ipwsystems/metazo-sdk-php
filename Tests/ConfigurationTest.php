<?php

namespace IpwSystems\MetazoApi\Tests;

use IpwSystems\MetazoApi\Configuration;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;

class ConfigurationTest extends TestCase
{
    public function testConfiguration()
    {
        $config = new Configuration([
            'endpoint' => 'https://test',
            'key' => 'x',
            'user' => 'xx',
            'password' => 'xxx',
        ]);

        $this->assertEquals('https://test/', $config->getEndpoint());
        $this->assertEquals(1, $config->getSite());
        $this->assertEquals('x', $config->getKey());
        $this->assertEquals('xx', $config->getUser());
        $this->assertEquals('xxx', $config->getPassword());
        $this->assertEquals('EN', $config->getLanguage());
    }

    public function testInvalidOption()
    {
        $this->expectException(InvalidOptionsException::class);

        $config = new Configuration([
            'endpoint' => 'https://test',
            'key' => 'x',
            'user' => 'xx',
            'password' => 'xxx',
            'site' => 'y'
        ]);
    }

    /**
     * @dataProvider missingOptionsProvider
     */
    public function testMissingOption(array $options) {
        $this->expectException(MissingOptionsException::class);

        $config = new Configuration($options);
    }

    public function missingOptionsProvider(): iterable
    {
        yield [[
            'key' => 'x',
            'user' => 'xx',
            'password' => 'xxx',
        ]];
        yield [[
            'endpoint' => 'https://test',
            'user' => 'xx',
            'password' => 'xxx',
        ]];
        yield [[
            'endpoint' => 'https://test',
            'key' => 'x',
            'password' => 'xxx',
        ]];
        yield [[
            'endpoint' => 'https://test',
            'key' => 'x',
            'user' => 'xx',
        ]];
    }
}
