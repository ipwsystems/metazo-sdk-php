<?php

namespace IpwSystems\MetazoApi\Tests;

use IpwSystems\MetazoApi\Client;
use IpwSystems\MetazoApi\Configuration;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    /**
     * @dataProvider optionsProvider
     */
    public function testClient(string $method, $expected, array $send = [], array $options = [])
    {
        $client = new ClientWrapper();
        $result = call_user_func_array([$client, $method], $options);
        // $result = $client->$method($options);

        $this->assertEquals($expected, $result);
        $this->assertEquals($send, $client->send);
    }

    public function optionsProvider(): iterable
    {
        // yield [
        //   <api method>,
        //   <expected result>,
        //   <expected request>,
        //   <api method params>
        // ]

        yield [
            'authenticate',
            true,
            [
                'method' => 'GET',
                'query' => [
                    'site' => '1',
                    'user' => 'xx',
                    'pass' => 'xxx',
                    'language' => 'EN',
                    'checksum' => '73bc83eef4ae90cc9489c8efbe23dfc09f48e64d',
                ],
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
            ]
        ];

        yield [
            'ping',
            'pong',
            [
                'method' => 'GET',
            ],
        ];

        yield [
            'datatypes',
            [
                'success' => true,
            ],
            [
                'method' => 'GET',
                'query' => [
                    'checksum' => '7abb9eab0e8cc1bf03326a766fc0e60a14d4aee8',
                    'token' => 'xx00xx',
                ],
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
            ]
        ];

        yield [
            'explain',
            [
                'success' => true,
            ],
            [
                'method' => 'GET',
                'query' => [
                    'datatype' => 'form123',
                    'checksum' => '42838e7be230bcefbb792f07210ec87e904507fb',
                    'token' => 'xx00xx',
                ],
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
            ],
            [
                'form123'
            ]
        ];

        yield [
            'model',
            [
                'success' => true
            ],
            [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'body' => '{"f001":"x","f002":"xx"}',
                'query' => [
                    'datatype' => 'form123',
                    'token' => 'xx00xx',
                    'checksum' => '91587df4848bfe4d56d12ab1bdf25dfbeafcc957',
                    'model' => 'create',
                ],
                'method' => 'POST',
            ],
            [
                'create',
                [
                    'datatype' => 'form123',
                    'f001' => 'x',
                    'f002' => 'xx',
                ]
            ]
        ];

        yield [
            'list',
            [
                'success' => true
            ],
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'query' => [
                    'datatype' => 'form123',
                    'search' => 'test',
                    'searchfield' => 'f001;f002',
                    'searchcomp' => 'EQUAL;LESS',
                    'searchandor' => 'OR',
                    'limit' => '2',
                    'checksum' => 'efcebdbe46303ab2a040dbe08a7767fed8ba38e0',
                    'token' => 'xx00xx'
                ],
                'method' => 'GET'
            ],
            [
                [
                    'datatype' => 'form123',
                    'search' => 'test',
                    'searchfield' => 'f001;f002',
                    'searchcomp' => 'EQUAL;LESS',
                    'searchandor' => 'OR',
                    'limit' => 2,
                ]
            ]
        ];

        yield [
            'list',
            [
                'success' => true
            ],
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'query' => [
                    'datatype' => 'form123',
                    'checksum' => '42838e7be230bcefbb792f07210ec87e904507fb',
                    'token' => 'xx00xx',
                ],
                'method' => 'GET'
            ],
            [
                [
                    'datatype' => 'form123',
                ]
            ]
        ];
    }
}
