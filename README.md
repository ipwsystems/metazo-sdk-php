PHP client for the IPW Metazo api.

https://metazoapi.support.ipw.dk/


## Installation

> composer require ipwsystems/metazo-sdk-php


## Usage

Setup

```php
use IpwSystems\MetazoApi\Client;
use IpwSystems\MetazoApi\Configuration;

$config = new Configuration([
    'endpoint' => 'https://.../metazo/v1/',
    'user' => 'api user',
    'password' => 'api user password',
    'key' => 'api key',
    'site' => 1
]);

$client = new Client($config);
```

Authentication

```php
if ($client->authenticate()) {
    // authenticated...
}
```

Here a `list` and a `model` request.

List

```php
$client->list('user', [
    'search' => 'Peter',
    'searchfield' => 'name',
    'search' => 'LIKEEND',
]);
```

Model

```php
$client->model('create', [
    'datatype' => 'form1234',
    'f001' => 'test',
    'f002' => 'test 1',
    'f003' => 'test 2',
]);

```

Here the full list of methods. See the api documentation for details.

```php
$client->ping(): string;
```

```php
$client->authenticate(): bool;
```

```php
$client->validateToken(): bool
```

```php
$client->revokeToken(): bool
```

```php
$client->datatypes(): array
```

```php
$client->explain(string $dataType): array
```

```php
$client->model(string $model, array $options): array

$options = [
    'datatype' => '',
    'model' => '',
    'objectId' => null,
];
```

```php
$client->list(string $datatype, array $options): array;

$options = [
    'datatype' => '',
    'search' => '',
    'searchfield' => '',
    'searchcomp' => '',
    'searchandor' => 'AND',
    'fields' => '',
    'limit' => 20,
    'offset' => 0,
];
```

```php
$client->read(int $objectId): array
```

```php
$client->uploadFiles(int $parentId, array $files): array

// Here `$files` is an array of full paths to files to upload and attach to an object.

```


```php
$client->downloadFile(int $objectId, string $destination): <data stream>
```

```php
$client->filter(int $filterId): array
```

```php
$client->filterCount(int $filterId): int
```
