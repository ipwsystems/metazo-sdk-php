<?php

namespace IpwSystems\MetazoApi;

use InvalidArgumentException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class Client
{
    private Configuration $configuration;
    protected string $token = '';
    private array $validSearchComp = [
        'LIKE',
        'LIKESTART',
        'LIKEEND',
        'GREATER',
        'GREATEREQUAL',
        'LESS',
        'LESSEQUAL',
        'EQUAL',
        'NOTEQUAL',
        'SOUNDSLIKE',
        'NOTSOUNDSLIKE',
        'NOTLIKE',
        'IN',
        'ISEMPTY',
        'ISTODAY',
        'BEFORETODAY',
        'AFTERTODAY',
        'DAYSAGO',
        'INDAYS',
        'LESSDAYSAGO',
        'NOTIN',
        'RLIKE',
        'FINDINSET',
        'ISNOTEMPTY',
        'INMOREDAYS',
    ];

    private array $validSearchAndOr = [
        'AND',
        'OR',
        '',
    ];

    private bool $verifyHost = true;

    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * Disable host verification.
     * Useful if you use self-signed certificates, otherwise don't.
     *
     * @return $this
     */
    public function disableHostVerification(): self
    {
        $this->verifyHost = false;
        return $this;
    }

    /**
     * Authenticate client.
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @return bool
     */
    public function authenticate(): bool
    {
        $this->token = '';
        $response = $this->query('authenticate', [
            'site' => $this->configuration->getSite(),
            'user' => $this->configuration->getUser(),
            'pass' => $this->configuration->getPassword(),
            'language' => $this->configuration->getLanguage(),
        ]);

        if ($response['token']) {
            $this->token = $response['token'];
            return true;
        }

        return false;
    }

    /**
     * Validate token.
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @return bool
     */
    public function validateToken(): bool
    {
        $response = $this->query('validate');

        return $response['success'];
    }

    /**
     * Revoke access to a token.
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @return bool
     */
    public function revokeToken(): bool
    {
        $response = $this->query('revoke');

        return $response['success'];
    }

    /**
     * Send A ping
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @return string
     */
    public function ping(): string
    {
        $response = $this->makeRequest('GET', $this->configuration->getEndpoint() . 'ping')->toArray();
        return $response['ping'] ?? '';
    }

    /**
     * List all available data types.
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @return array
     */
    public function datatypes(): array
    {
        return $this->query('datatypes');
    }

    /**
     * Explain a data type.
     *
     * @param string $dataType
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @return array
     */
    public function explain(string $dataType): array
    {
        return $this->query('explain', [
            'datatype' => $dataType,
        ]);
    }

    /**
     * Read a model.
     *
     * @param string $model
     * @param array $options
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @return array
     */
    public function model(string $model, array $options): array
    {
        $options['model'] = $model;

        if (empty($model)) {
            throw new InvalidArgumentException('You must provide a model.');
        }

        if (empty($options['datatype'])) {
            throw new InvalidArgumentException('You must provide a "datatype" option.');
        }

        if ('create' !== $model && !is_int($options['objectid'])) {
            throw new InvalidArgumentException('You must provide an "objectid" option for the "' . $model . '" model.');
        }

        return $this->query('model', $options);
    }

    /**
     * List objects matching a set of search filters.
     *
     * @param string|array $name
     * @param array $options
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @return array
     */
    public function list($name, array $options = []): array
    {
        if (is_array($name)) {
            $options = $name;
        } else {
            $options['datatype'] = $name;
            unset($name);
        }

        if (empty($options['datatype'])) {
            throw new InvalidArgumentException('You must provide a "datatype" option.');
        }

        if (array_key_exists('searchfield', $options)) {
            if (empty($options['searchfield'])) {
                throw new InvalidArgumentException('You must provide a "searchfield" option.');
            }

            if (empty($options['search'])) {
                throw new InvalidArgumentException('You must provide a "search" option.');
            }

            if (array_key_exists('searchcomp', $options)) {
                $validSearchComp = $this->validSearchComp;
                $searchComp = explode(';', $options['searchcomp']);
                array_walk($searchComp, static function ($comp) use ($validSearchComp) {
                    if (!in_array($comp, $validSearchComp, true)) {
                        throw new InvalidArgumentException('Invalid searchcomp: ' . $comp);
                    }
                });
            } else {
                $options['searchcomp'] = 'EQUAL';
            }

            if (array_key_exists('searchandor', $options)) {
                $validSearchAndOr = $this->validSearchAndOr;
                $searcAndOr = explode(';', $options['searchandor']);
                array_walk($searcAndOr, static function ($comp) use ($validSearchAndOr) {
                    if (!in_array($comp, $validSearchAndOr, true)) {
                        throw new InvalidArgumentException('Invalid searchandor: ' . $comp);
                    }
                });
            } else {
                $options['searchandor'] = 'AND';
            }
        } else {
            $fields = [
                'search',
                'searchfield',
                'searchcomp',
                'searchandor',
            ];
            $options = array_filter($options, fn($field) => !in_array($field, $fields), ARRAY_FILTER_USE_KEY);
        }

        if (
            array_key_exists('limit', $options)
            && (
                !is_numeric($options['limit'])
                || $options['limit'] < 1
            )
        ) {
            unset($options['limit']);
        }

        if (
            array_key_exists('offset', $options)
            && (
                !is_numeric($options['offset'])
                || $options['offset'] < 0
            )
        ) {
            unset($options['offset']);
        }

        return $this->query('list', $options);
    }

    /**
     * Read an object.
     *
     * @param int $objectId
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @return array
     */
    public function read(int $objectId): array
    {
        return $this->query('read', [
            'objectid' => $objectId,
        ]);
    }

    /**
     * Upload files and attach these to a parent.
     *
     * @param int $parentId
     * @param array $files
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @return array
     */
    public function uploadFiles(int $parentId, array $files)
    {
        $getParts = [
            'parentid' => $parentId,
        ];

        $index = 1;
        $formParts = [];
        foreach ($files as $file) {
            $key = "file_{$index}";
            $formParts[$key] = DataPart::fromPath($file);
            $getParts[$key] = $this->fileChecksum($file);
            $index++;
        }

        $getParts['token'] = $this->token;
        $getParts['checksum'] = $this->generateChecksum($getParts);

        $formData = new FormDataPart($formParts);
        $url = $this->configuration->getEndpoint() . 'binfile/upload?' . http_build_query($getParts);

        return $this->makeRequest('POST', $url, [
            'headers' => $formData->getPreparedHeaders()->toArray(),
            'body' => $formData->bodyToIterable(),
        ])->toArray();
    }

    /**
     * Download a file.
     *
     * @param int $objectId
     * @param string $destination
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @return bool
     */
    public function downloadFile(int $objectId, string $destination)
    {
        /** @var ResponseInterface $response */
        $response = $this->query('binfile/download', [
            'objectid' => $objectId,
        ], [], true);

        if ($response->getStatusCode() !== 200) {
            return false;
        }

        $source = $response->toStream();
        $target = fopen($destination, 'w');
        $bytes = stream_copy_to_stream($source, $target);
        fclose($target);
        fclose($source);

        return $bytes > 0;
    }

    /**
     * Get records from a filter.
     *
     * @param int $filterId
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @return array
     */
    public function filter(int $filterId): array
    {
        return $this->query("filter/{$filterId}");
    }

    /**
     * Get object count from a filter.
     *
     * @param int $filterId
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @return int
     */
    public function filterCount(int $filterId): int
    {
        $response = $this->query("filter/{$filterId}/count");

        return (int) $response['count'];
    }

    /**
     * Build and run query.
     *
     * @param string $method
     * @param array $params
     * @param array $headers
     * @param bool $asResponse
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @return array|ResponseInterface
     */
    protected function query(string $method, array $params = [], array $headers = [], bool $asResponse = false) {
        $requestMethod = 'GET';
        if (in_array($method, ['model', 'binfile/upload'], true)) {
            $requestMethod = 'POST';
        }

        if ($this->token) {
            $params['token'] = $this->token;
        }

        $params['checksum'] = $this->generateChecksum($params);

        if (empty($headers['Content-Type'])) {
            $headers['Content-Type'] = 'application/json';
        }

        $options = [
            'headers' => $headers,
        ];

        $url = $this->configuration->getEndpoint() . $method;

        if ('POST' === $requestMethod) {
            // These needs to be GET variables...
            $query = [
                'datatype' => $params['datatype'],
                'token' => $params['token'],
                'checksum' => $params['checksum'],
            ];

            unset($params['datatype'], $params['token'], $params['checksum']);

            if (isset($params['model'])) {
                $query['model'] = $params['model'];
                unset($params['model']);
            }

            if (isset($params['objectid'])) {
                $query['objectid'] = $params['objectid'];
                unset($params['objectid']);
            }

            $options['body'] = json_encode($params);
            $params = $query;
        }

        if (count($params)) {
            $url .= '?' . http_build_query($params);
        }

        $response = $this->makeRequest($requestMethod, $url, $options);
        if ($asResponse) {
            return $response;
        }

        $data = $response->toArray();

        $data["success"] = $data["success"] === "true";
        return $data;
    }

    /**
     * Make the actual request.
     *
     * @param string $requestMethod
     * @param string $url
     * @param array $options
     * @throws TransportExceptionInterface
     * @return ResponseInterface
     */
    protected function makeRequest(string $requestMethod, string $url, array $options = [])
    {
        $client = HttpClient::create();
        if (!$this->verifyHost) {
            $options['verify_host'] = false;
            $options['verify_peer'] = false;
        }

        return $client->request($requestMethod, $url, $options);
    }

    /**
     * Generate checksum from an array of data.
     *
     * @param array $input
     * @return string
     */
    private function generateChecksum(array $input): string {
        ksort($input);

        $string = '';
        foreach($input as $key => $val) {
            $string .= $key . $val;
        }

        return hash_hmac("SHA1", $string, $this->configuration->getKey());
    }

    /**
     * Generate file checksum as defined in the api docs.
     *
     * @param string $file
     * @return string
     */
    private function fileChecksum(string $file): string
    {
        return hash('sha1', file_get_contents($file, false, null, 0, 256));
    }
}
