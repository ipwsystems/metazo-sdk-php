<?php

namespace IpwSystems\MetazoApi;

use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Configuration
{
    private array $options;

    /**
     * @param array{
     *   endpoint: string,
     *   key: string,
     *   user: string,
     *   password: string ,
     *   site: int, ,
     *   language: string
     * } $options
     */
    public function __construct(array $options) {
        $resolver = new OptionsResolver();
        $resolver->setDefaults([
            'site' => 1,
            'language' => 'EN',
        ]);

        $resolver->setRequired(['endpoint', 'key', 'user', 'password', 'site']);
        $resolver->setAllowedTypes('endpoint', 'string');
        $resolver->setAllowedTypes('key', 'string');
        $resolver->setAllowedTypes('user', 'string');
        $resolver->setAllowedTypes('password', 'string');
        $resolver->setAllowedTypes('site', 'int');
        $resolver->setAllowedTypes('language', 'string');

        $resolver->setAllowedValues('endpoint', static function ($value) {
            return filter_var($value, FILTER_SANITIZE_URL);
        });

        $resolver->setNormalizer('endpoint', static function (Options $options, $value) {
            if (substr($value, -1) !== '/') {
                $value .= '/';
            }
            return $value;
        });

        $this->options = $resolver->resolve($options);
    }

    public function getEndpoint(): string
    {
        return $this->options['endpoint'];
    }

    public function getKey(): string
    {
        return $this->options['key'];
    }

    public function getUser(): string
    {
        return $this->options['user'];
    }

    public function getPassword(): string
    {
        return $this->options['password'];
    }

    public function getSite(): int
    {
        return $this->options['site'];
    }

    public function getLanguage(): string
    {
        return $this->options['language'];
    }
}
